#!/usr/bin/python3
# -*- coding: utf-8 -*-

import calcoohija
import sys
import csv

if __name__ == "__main__":

    with open(sys.argv[1], newline='') as fichero:

        lineas = csv.reader(fichero, delimiter=",")
        for info in lineas:
            operador = info[0]
            result = info[1]
            for numbers in info[2:]:
                calcu = calcoohija.CalculadoraHija(operador, result, numbers)
                result = calcu.operar()
            print(result)
