#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def mult(self):

        return float(self.op1) * float(self.op2)

    def div(self):

        if self.op2 == 0:
            return("Division by zero is not allowed")
        else:
            return float(self.op1) / float(self.op2)

    def operar(self):

        if self.operador == "multiplicación":
            return self.mult()
        elif self.operador == "división":
            return self.div()
        else:
            return calcoo.Calculadora.operar(self)


if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit("$ python3 calcoo.py operando1 operación operando2")

    operador = sys.argv[2]
    op1 = sys.argv[1]
    op2 = sys.argv[3]
    try:
        op1 = float(op1)
        op2 = float(op2)
    except ValueError:
        sys.exit("Operands must be numbers")

    calcu = CalculadoraHija(operador, op1, op2)
    resultado = calcu.operar()
    print(resultado)
