#!/usr/bin/python3
# -*- coding: utf-8 -*-

import calcoohija
import sys

if __name__ == "__main__":

    fichero = open(sys.argv[1])
    lineas = fichero.readlines()

    for info in lineas:
        coma = info.split(",")
        operador = coma[0]
        result = coma[1]
        for numeros in coma[2:]:
            calcu = calcoohija.CalculadoraHija(operador, result, numeros)
            result = calcu.operar()
        print(result)
