#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():

    def __init__(self, operador, op1, op2):

        self.operador = operador
        self.op1 = op1
        self.op2 = op2

    def suma(self):

        return float(self.op1) + float(self.op2)

    def resta(self):

        return float(self.op1) - float(self.op2)

    def operar(self):

        if self.operador == "suma":
            return self.suma()
        elif self.operador == "resta":
            return self.resta()
        else:
            return("Operación no válida")


if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit("$ python3 calcoo.py operando1 operación operando2")

    operador = sys.argv[2]
    op1 = sys.argv[1]
    op2 = sys.argv[3]
    try:
        op1 = float(op1)
        op2 = float(op2)
    except ValueError:
        sys.exit("Los operandos deben ser números")

    calcu = Calculadora(operador, op1, op2)
    resultado = calcu.operar()
    print(resultado)
